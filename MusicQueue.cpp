#include<iostream>
using namespace std;

class Node {
public:
		string Title;
		string Artist;
		Node*	next;
};

class Queue {

public:

Queue() { // constructor
front = rear = NULL;
counter = 0;
}

~Queue() {// destructor
while (!IsEmpty()) Dequeue();
}

bool IsEmpty() {
	if (counter)
		return false;
	else
		return true;
}

void Enqueue(string title,string artist);
bool Dequeue();
void DisplayQueue(void);
private:
Node* front;
// pointer to front node
Node* rear;
// pointer to last node
int counter;
// number of elements
};

void Queue::Enqueue(string title,string artist) {
	Node* newNode = new Node;
	newNode->Title = title;
	newNode->Artist = artist;
	
	newNode->next = NULL;
	
	if (IsEmpty()) {
		front = newNode;
		rear = newNode;
	}
	
	else {
		rear->next = newNode;
		rear = newNode;
	}
	counter++;
}

bool Queue::Dequeue() {

if (IsEmpty()) {
cout << "Error: the queue is empty." << endl;
return false;
}

else {
Node* nextNode = front->next;
delete front;
front = nextNode;
counter--;
}

}

void Queue::DisplayQueue() {
	cout << "TOP OF QUEUE --->";
	Node* currNode = front;
	for (int i = 0; i < counter; i++) {
		if (i == 0) cout << "\t";
		else
		cout << "\t\t\t";
		cout << currNode->Title<<" by ";
		cout<< currNode ->Artist;
		if (i != counter - 1)
		cout << endl;
		else
		cout << "\t<--- LAST IN QUEUE" << endl;
		currNode = currNode->next;
	}
}

Queue Music;

int switchHandler(int choice){
	string Title,Artist;
	cout<<"1.) Add a Music\n2.) Delete a Music\n3.) View Playlist\n";
	cout<<"What would you like to do? > ";
	cin>>choice;
	if(cin.fail()){
			cout<<"Invalid Input.\n";
			cin.clear();
			cin.ignore();
			system("pause");
		}
	else{
		switch(choice){
			case 1://Enqueue
				cout<<"Enter Title of the Music to be added > ";
				cin.ignore();
				getline(cin,Title);
				cout<<"Enter Artist > ";
				getline(cin,Artist);
				Music.Enqueue(Title,Artist);
				cout<<"Current Playlist : \n";
				Music.DisplayQueue();
			break;
			case 2://Dequeue
				Music.DisplayQueue();
				system("pause");
				Music.Dequeue();
				cout << "Last one deleted" << endl;
				system("CLS");
				Music.DisplayQueue();
			break;
			case 3://Display
				Music.DisplayQueue();
				break;
			default:
				cout<<"No such option.\n";
			break;
		}
	}
}


int main(){
	int choice;
	char yesSwitch = 'y';
	Music.Enqueue("Mundo" , "IV of Spades");
	Music.Enqueue("Buwan", "Juan Karlos Labajo");
	Music.Enqueue("Be My Mistake ","The 1975");
	Music.Enqueue("Sayo","FourPlay MNL");
	Music.Enqueue("Killer in the Mirror "," Set it Off");
	while((yesSwitch == 'Y')||(yesSwitch == 'y')){
			system("CLS");
		switchHandler(choice);
		cout<<"Would you like to stay?(y/n) > ";
		cin>>yesSwitch;
	}
	cout<<"Come Again!\n";
}



