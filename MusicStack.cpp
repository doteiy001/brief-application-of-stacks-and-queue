#include <iostream>
#include <conio.h>
#include <list>
using namespace std;

class Node {
public:
		string	Title;
		string	Artist;
		int Index;
		Node*	next;
};
class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* Add(int index,string Title, string Artist);
			int Find(string x);
			int Delete(int index);
			void DisplayList(void);
			void Edit(int index, string newTitle, string Artist);
			string ViewMusic(int index);
			int Size(Node* index);
	private:
			Node* head;
			Node* artist;
};
int List::Find(string x){
	Node* CurrNode	=	head;
	Node* CurrArtist =	artist;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->Title != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}

int List::Size(Node* index){
    int count = 0; // Initialize count  
    Node* current = head; // Initialize current  
    while (current != NULL)  
    {  
        count++;  
        current = current->next;  
    }  
    return count;  
}  

int List:: Delete(int index){
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	Node* prevArtist = NULL;
	Node* CurrArtist = artist;
	int CurrIndex	=	1;
	while (CurrIndex != index){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		prevArtist = CurrArtist;
		CurrArtist = CurrArtist->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode && prevArtist){
			prevNode->next	=	CurrNode->next;
			prevArtist->next = CurrArtist->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			artist = CurrArtist->next;
			delete	CurrNode;
			delete	CurrArtist;
		}
		return	CurrIndex;
	}
	return 0;
}
string List::ViewMusic(int index){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->Title;
	//return NULL;
}

void List::DisplayList(){
	int num = 0;
	Node* CurrNode	=	head;
	Node* CurrArtist = 	artist;
	while (CurrNode != NULL &&CurrArtist != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->Title<<" ";
		cout<<" "<<"by "<<" "<<CurrArtist->Artist<<endl;;
		CurrArtist = CurrArtist->next;
		CurrNode	=	CurrNode->next;
		num++;
	}
}

Node* List::Add(int index,string Title, string Artist){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	Node* CurrArtist = artist;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
		CurrArtist	=	CurrArtist->next;
	}
	Node* newNode =		new		Node;
	newNode->Title = 	Title;
	Node* newArtist = new Node;
	newArtist->Artist = Artist;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
			newArtist->next = artist;
			artist = newArtist;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
		newArtist->next = CurrArtist->next;
		CurrArtist->next = newArtist;
	}
	return newNode;
}


List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}

class Stack{
	public:
		Nullifier();
		void Push(string Title,string Artist);
		void Pop();
		void Top(string Title, string Artist);
		void Display();
		void viewPlaylist();// in view menu
			int addMusic(int add, int count); //add to playlist
			int removeMusic(int count);  // remove from playlist
	private:
		struct Node // struct
		{
			Node* next;
			int data, Playlist; 
			string Title;
			string Artist;
			bool checker;	
		} typedef *pointer; // set as pointer datatype
		pointer head;
		pointer curr;
		pointer top;
		pointer temp;
};

Stack::Nullifier()
{
	temp = NULL;
	head = NULL;
	curr = NULL;
	top = NULL;
}
void Stack::Top(string Title,string Artist){
			if(curr == NULL||curr== 0 ){
				cout<<"Stack is Empty.\n";
			}
			else{
				curr = top;
				cout<<curr->Title<<" by "<<curr->Artist<<endl;
			}
			
			
		}

void Stack::Push(string Title, string Artist) //push stacks
{
	pointer link = new Node; // creating new node
	link->next = NULL; //no data
		if(head!=NULL) //empty head
			{
			curr = head;
			temp = head;
			while(curr->next != NULL) 
			{
				curr = curr->next;
				temp = curr;
			}
			curr->next = link;
			curr = curr->next;
			curr->data = temp->data+1; // "pushing" node / adding to stacks
			}
		else
		{
			head = link; //make the new node the head
			link->data = 0; //back to null
		}
	top = link; //top of stack
	link->Title = Title; //to title
	link->Artist = Artist;//to artist
	link->checker = false; //to check
	link->Playlist = 0; //playlist
}

void Stack::Pop() //delete stacks
{
	curr=head; 
	temp=head;
//	int newTopData;
	if(curr->next==NULL||curr==NULL) //check if empty stack
	{
		cout<<"Your Playlist is empty."<<endl;
	}
	else
	{
		while(curr->data!=top->data-1) //loop for checking the num of stacks
		{
			temp=curr;
			curr=curr->next;
		}
		if(curr->data==0) //empty 
		{
			curr->next=NULL;
			top=curr;
			cout<<"No more music in the Playlist."<<endl;	
		}
		else //remover
		{
			top=curr;
			curr->next = NULL;
		}	
	}
}

void Stack::Display() //display list of songs
{
	cout << "  Music List " << endl;
	for(int i = top->data ;i>=0 ;i--)	
	{
		curr=head;
		temp=head;
		while(curr!=NULL &&curr->data!=i) //stack is not empty
		{
			curr = curr->next;
		}
		if(curr==NULL)
		{
		}
		else
		{
			cout << curr->data+1 << ") " << curr->Title << " by " << curr->Artist<<endl;
		}
	} 
	cout << "" << endl;
	cout << "Number of Songs: " << top->data+1 <<endl;
	cout << "" << endl;
}

void Stack::viewPlaylist() //display playlist
{
	for(int i = top->data;i>=0;i--)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->Playlist!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->data==0||curr->Playlist==0||curr->checker==false)
		{
		}
		else
		{
			cout << curr->Playlist << ") " << curr->Title << endl;
		}
	}
}

int Stack::addMusic(int add, int count) //add to playlist
{
	pointer answer = NULL;
	curr=head;
	temp=head;
	while(curr != NULL && curr-> data != add) 
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data == 0) // if number not on the list
	{
		cout << "The music number " << add << " is not on the playlist." << endl;
		delete answer;
		return 0;
	}
	else if(curr->checker==true) // delete if true
	{
		return 0;
	}
	else
	{	
		curr->Playlist = count;
		curr->checker = true;
		return 1; 
	}
}

int Stack::removeMusic(int count)
{
	int delMusic = count -1;
		pointer answer = NULL; 
		curr=head;
		temp=head;
		while(curr != NULL && curr-> Playlist != delMusic)
		{
			temp = curr;
			curr = curr->next;
		}
		if (curr == NULL or curr->data ==0)
		{
			delete answer;
			return 0;
		}
		else if(curr->checker==false)
		{
			return 0;
		}
		else
		{	
			curr->Playlist = 0;
			curr->checker =false;
			return 1; 
		}
}

Stack Music;

int switchHandler(int choice){
	Node* CurrNode;
	string Title,Artist,editName,searchName;
	int delMusic,editNumber,playNumber,musicSize;
	//cout<<"The Playlist Currently has "<<Music.Size(CurrNode)<<" elements.\n";
	system("CLS");
	cout<<"1.) Add a Music(push)\n2.) Delete a Music(pop)\n3.) Play Music(top)\n4.) View Playlist\n";
	cout<<"What would you like to do? > ";
	cin>>choice;
	if(cin.fail()){
			cout<<"Invalid Input.\n";
			cin.clear();
			cin.ignore();
			system("pause");
		}
	else{
		switch(choice){
			case 1://Add
				cout<<"Enter Title of the Music to be added > ";
				cin.ignore();
				getline(cin,Title);
				cout<<"Enter Artist > ";
				getline(cin,Artist);
				Music.Push(Title,Artist);
				cout<<"Current Playlist : \n";
				Music.Display();
			break;
			case 2://Delete
				Music.Display();
				cout<<"A Song has been Removed.\n";
				Music.Pop();
				Music.Display();
			break;
			case 3:
				cout<<"Now Playing : ";
				Music.Top(Title,Artist);
				cout<<"Current Playlist is : "<<endl;
				Music.Display();
			break;
			case 4://view
				cout<<"Current Playlist is : "<<endl;
				Music.Display();
			break;
			default:
				cout<<"No such option.\n";
			break;
		}
	}
}

int main(){
	Node* CurrNode;
	int choice;
	char yesSwitch = 'y';
	Music.Push("Mundo","IV of Spades");
	Music.Push("Buwan","Juan Karlos Labajo");
	Music.Push("Be My Mistake","The 1975");
	Music.Push("Sayo","FourPlay MNL");
	Music.Push("Killer in the Mirror", "Set it Off");
	while((yesSwitch == 'Y')||(yesSwitch == 'y')){
		system("CLS");
		switchHandler(choice);
		cout<<"Would you like to stay?(y/n) > ";
		cin>>yesSwitch;
	}
	cout<<"Come Again!\n";
}
